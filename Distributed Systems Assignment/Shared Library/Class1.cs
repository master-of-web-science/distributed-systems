﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared_Library
{
    public interface INews
    {
        // News Abstract Functions
        int AddNews(News news);
        void DeleteNews(int id);
        void UpdateNews(string photo);
        News[] GetAllNews();
        News GetSpicficAgencyNews(int agencyId);

        // Agency Abstract Functions
        int AddAgency(Agency agency);
        void removeAgency(int agencyId); 
        Agency GetAgency(int agencyId);
        void ModifyAgency(Agency agency);
    }
    
    [Serializable]
    public class News
    {
        public int id;
        public int agencyId;
        public DateTime date;
        public String title;
        public String newsAbstract;
        public String text;
        public String photo;
        public int totalReads;
        public int ranking;
        public News()
        {
            Console.WriteLine("{0}:{1}:{2}:{3}",
                DateTime.Now.Hour.ToString(),
                DateTime.Now.Minute.ToString(),
                DateTime.Now.Second.ToString(),
                DateTime.Now.Millisecond.ToString());
            Console.WriteLine("News Constructer - Object Created Sucessfully :), what a nice object");
        }

    }
    [Serializable]
    public class Agency
    {
        public int id;
        public String city;
        public String language;
        public Agency()
        {
            Console.WriteLine("{0}:{1}:{2}:{3}",
               DateTime.Now.Hour.ToString(),
               DateTime.Now.Minute.ToString(),
               DateTime.Now.Second.ToString(),
               DateTime.Now.Millisecond.ToString());
            Console.WriteLine("Agency Constructer - Object Created Sucessfully :), what a nice object");
        } 


    }
}
